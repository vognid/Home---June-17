// $(document).ready(function(){
//   var delay=1000, setTimeoutConst;
//   $('#a').hover(function(){
//    setTimeoutConst = setTimeout(function(){
//      /* Do Some Stuff*/
//       alert('Thank You!');
//    }, delay);
//   },function(){
//    clearTimeout(setTimeoutConst );
//   });
// })

var basicScrollTop = function () {  
  // The button
  var btnTop = document.querySelector('.goTop');
  // Reveal the button
  var btnReveal = function () { 
    if (window.scrollY >= 300) {
      btnTop.classList.add('is-visible');
    } else {
      btnTop.classList.remove('is-visible');
    }    
  }  
  var TopscrollTo = function () {
    if(window.scrollY!=0) {
      setTimeout(function() {
        window.scrollTo(0,window.scrollY-25);
        TopscrollTo();
      }, 15);
    }
  }
  // Listeners
  window.addEventListener('scroll', btnReveal);
  btnTop.addEventListener('click', TopscrollTo);  
    
};
basicScrollTop();


$(".theme-switch").on("click", () => {
  $("body").toggleClass("light-theme");
  document.body.classList.toggle("light-theme2");
});
